# Spring Boot Admin Server 

## Overview

Spring Boot Admin is a simple application to manage and monitor your Spring Boot Applications. The applications register with our Spring Boot Admin Client (via http) or are discovered using Spring Cloud (e.g. Eureka). The UI is just an Angular.js application on top of the Spring Boot Actuator Endpoints. In case you want to use the more advanced features (e.g. jmx-, loglevel-management), Jolokia must be included in the client application.

### Running Applicaiton ###

After cloning the GIT repository, open the command window (Windows) or terminal (MAC), change directory into the root of the application and run the following command:

```
mvn clean package # generates the executable jar  
```

### Starting the application
```
mvn spring-boot:run # this will start the application at port 8080 
```

application is running at 7777 port at the moment , if we want to run at different port , please change in application.properties

```
server.port
```
property

### Register client applications ###

Each application that want to register itself to the admin has to include the Spring Boot Admin Client.

Add spring-boot-admin-starter-client to your dependencies:
in pom.xml add
```
<dependency>
    <groupId>de.codecentric</groupId>
    <artifactId>spring-boot-admin-starter-client</artifactId>
    <version>1.2.5</version>
</dependency>

```
Note: version may need to change depending on the spring boot version you are using, the latest one available is 1.3.2 / 1.3.3.

Trigger the contained AutoConfiguration and tell the client where to find the admin to register at:

application.properties
```
spring.boot.admin.url=http://localhost:7777				 
```		    
#### Show version in application list ###

To get the version show up in the admin application list you have to set info.version. For example using maven filtering during the build:

application.properties
info.version=@project.version@

#### Show Application Name in application list ###

spring.application.name=NameOfTheApplication